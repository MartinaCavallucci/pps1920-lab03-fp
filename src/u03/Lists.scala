package u03

import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u02.Optionals.Option
import u02.Optionals.Option.Some


object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), n) if n>0 => drop(t, n-1)
      case _ => l
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case _ => Nil()
    }
    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h,t) => append(f(h),flatMap(t)(f))
      case _ => Nil()
    }
    def map2[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h,t) => Cons(mapper(h),flatMap(t)(v=>Cons(mapper(v),Nil())))
      case _ => Nil()
    }
    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    @scala.annotation.tailrec
    def filter2[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, flatMap(t)(v=>Cons(v,Nil())))
      case Cons(_,t) => filter2(t)(pred)
      case Nil() => Nil()
    }
    /* Max with math.max operation
    def max(l: List[Int]): Option[Int]  = l match {
      case Cons(h,t) => Some(math.max(h,Option.getOrElse(max(t),0)))
      case Cons(h,Nil())=> Some(h)
      case _ => Option.None()
    }*/
    def max(list: List[Int]): Option[Int] = {
      @scala.annotation.tailrec
      def maxRec(currentMax: Int, l: List[Int]): Option[Int] = l match{
        case Cons(head, tail) => maxRec(head.max(currentMax), tail)
        case _ => Some(currentMax)
      }
      maxRec(0, list)
    }
    def takeCourses[B](list:List[Person]):List[String] = map(filter(list)(p => p.isInstanceOf[Teacher]))(p=>p.asInstanceOf[Teacher].course)


    @scala.annotation.tailrec
    def foldLeft[A,B](list: List[A])(acc: B)(f: (B, A) => B): B = list match {
      case Cons(h,t) => foldLeft(t)(f(acc, h))(f)
      case Nil() => acc
    }
    //TODO
    def reverse[A](list:List[A]):List[A] = foldLeft(list)(Nil():List[A])((x,y)=>Cons(y,x))
    def foldRight[A,B](list: List[A])(acc: B)(f: (B, A) => B): B = foldLeft(reverse(list))(acc)(f)
  }
}

object ListsMain extends App {
  import u03.Lists.List._
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  println(append(Cons(5, Nil()), l)) // 5,10,20,30

  /* Test drop operation */
  val lst = Cons (10 , Cons (20 , Cons (30 , Nil () ) ) )
  println(drop ( lst ,1)) // Cons (20 , Cons (30 , Nil () ) )
  println(drop ( lst ,2)) // Cons (30 , Nil () )
  println(drop ( lst ,5)) // Nil ()

  /* Test flatMap operation */
  println(flatMap(lst)(v=>Cons(v + 1, Nil()))) // Cons(11,Cons(21,Cons(31,Nil())))
  println(flatMap(lst)(v=>Cons(v + 1, Cons(v + 2, Nil()))))//Cons(11,Cons(12,Cons(21,Cons(22,Cons(31, Cons(32, Nil()))))))
  /* Test map2 operation */
  println(map(lst)(v=>Cons(v + 1, Nil()))) // Cons(Cons(11,Nil()),Cons(Cons(21,Nil()),Cons(Cons(31,Nil()),Nil())))
  println(map(lst)(v=>Cons(v + 1, Cons(v + 2, Nil())))) //Cons(Cons(11,Cons(12,Nil())),Cons(Cons(21,Cons(22,Nil())),Cons(Cons(31,Cons(32,Nil())),Nil())))
  println(map2(lst)(v=>Cons(v + 1, Nil())))// Cons(Cons(11,Nil()),Cons(Cons(21,Nil()),Cons(Cons(31,Nil()),Nil())))
  println(map2(lst)(v=>Cons(v + 1, Cons(v + 2, Nil())))) //Cons(Cons(11,Cons(12,Nil())),Cons(Cons(21,Cons(22,Nil())),Cons(Cons(31,Cons(32,Nil())),Nil())))
  /* Test filter2 operation */
  println(filter[Int](lst)(_ >=20)) // Cons(20,Cons(30,Nil()))
  println(filter2[Int](lst)(_ >=20)) // Cons(20,Cons(30,Nil()))
  /* Test max operation */
  println(max( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) )) // Some (25)
  println(max( Nil () )) // None ()
  /* Test takeCoursesTeachers */
  val list = List.Cons[Person](Teacher("Viroli", "PPS"), List.Cons(Teacher("Mirri", "ASW"), List.Cons(Student("Mario", 20),Nil())))
  println(takeCourses(list))
  /*Test foldRight and foldLeft */
  val lsti = Cons(3,Cons(7,Cons(1,Cons(5,Nil()))))
  println(reverse(lsti))
  println(foldLeft ( lsti ) (0) (_ - _ ) )// -16
  println(foldRight ( lsti ) (0) (_ - _ )) // -8

}